# PHPStan: Finding bugs in your code without even running it
## Gabriel Caruso (@carusogabriel)

- Compiled vs Interpreted languages;
- 4 tools modern PHP development
  - PHP lint - check if your code is right;
  - PHP CodeSniffer - Checks if your code is readable;
  - PHPUnit;
  - PHPStan;
