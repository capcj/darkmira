# Open/Closed Principle: Goig Further, it's extends, not inherit!
## Thomas Dutrion @tdutrion

- SOLID Principles
  - Do not use separately, it's a set of principles
- Open/Closed Principle
  - Open
    - The developer has the ability to change the behaviour or/and state of component;
  - Closed
    - The developer cannot modify;
  - Good example are open source projects;
  - Bertrand Meyer: Inheritance (as in extends) in the 80's;
  - Inheritance is not usually the solution;
  - Design Patterns for extensions
    - Decorator pattern
    - Observer pattern
      - Allows as much extra code as we want (plugins)
      - Usually used together with a mediator (like an EventManager)
