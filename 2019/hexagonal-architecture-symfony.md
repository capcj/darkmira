# Hexagonal Architecture Symfony
## Lucas Cavalcante (@lucascgomes)

- Software Architecture is all about how you organize your application;
- Architecture is framework/language/lib agnostic;
- Domain is a way to refer the entire business logic, rules, process or properties
  - Its about what the system do;
- Clean Architecture (Robert C. Martin)
  - Software design philosophy that resembles a lot hexagonal architecture
  - Organizes the design elements by many levels;
- Onion Architecture (Jeffrey Pallermo);
- Hexagonal Architecture (Alistair Cockburn);
  - A different view to Ports and Adapters (Dependency Inversion Principle);
  - We must build our software based in its abstractions, not the implementations;
  - Ports are interfaces that exposes to the world the internal entities;
- Symfony architecture
  - Essential Things
    - Interfaces;
    - Entities;
    - Services;
